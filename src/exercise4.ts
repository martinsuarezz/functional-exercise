export const wordCount = (sequence: string): Record<string, number> => {
  return wordCountList(sequence.split(" ").filter(filterBlanks).map(convertToLowerCase))
};

const convertToLowerCase = (word: string): string => word.toLowerCase()

const filterBlanks = (word: string): Boolean => word != ''

const wordCountList = (wordList: string[]): Record<string, number> => {
  let words = {};
  return wordList.reduce(countWord, words);
}

const countWord = (words: Record<string, number>, word: string): Record<string, number> => {
  if (word in words) words[word] += 1;
  else words[word] = 1;
  return words;
}